import React, { Fragment } from 'react';
import { Segment, Comment, Header, Form, Button } from 'semantic-ui-react';

 const ActivityDetailedChat = () => {
    return (
        <Fragment>
            <Segment textAlign='center' attached='top' inverted color='teal' style={{ border: 'none' }} >
                <Header> Chat about this event</Header>
            </Segment>

            <Segment attached>
                <Comment.Group>
                    <Comment>
                        <Comment.Avatar src='/assets/user.png' />
                        <Comment.Content>
                            <Comment.Author as='a'> Matt</Comment.Author>
                            <Comment.Metadata>
                                <div> Today at 5:42pm </div>
                            </Comment.Metadata>
                            <Comment.Text> How Artistic </Comment.Text>
                            <Comment.Actions>
                                <Comment.Action> Reply</Comment.Action>
                            </Comment.Actions>
                        </Comment.Content>
                    </Comment>
                    <Comment>
                        <Comment.Avatar src='/assets/user.png' />
                        <Comment.Content>
                            <Comment.Author as='a'> Matt</Comment.Author>
                            <Comment.Metadata>
                                <div> Today at 5:42pm </div>
                            </Comment.Metadata>
                            <Comment.Text> How Artistic </Comment.Text>
                            <Comment.Actions>
                                <Comment.Action> Reply</Comment.Action>
                            </Comment.Actions>
                        </Comment.Content>
                    </Comment>

                    <Form reply>
                        <Form.TextArea />
                        <Button content = 'Add Reply' 
                            labelPosition = 'left'
                            icon='edit' 
                            primary />
                    </Form>
                </Comment.Group>
            </Segment>
        </Fragment>
    )
}

export default ActivityDetailedChat;