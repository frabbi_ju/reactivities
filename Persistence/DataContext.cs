﻿using System;
using Microsoft.EntityFrameworkCore;

using Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace Persistence
{
    public class DataContext : IdentityDbContext<AppUser>
    {
        public DataContext(DbContextOptions options) : base(options)

        {
        }

        public DbSet<Values> Values { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<PersonalInfo> PersonalInfos { get; set; }
        public DbSet<UserActivity> UserActivities { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {

            //base.OnModelCreating(builder);
            builder.Entity<IdentityUserLogin<string>>(eb =>
            {
                eb.HasNoKey();
            });
            builder.Entity<IdentityUserRole<string>>(eb =>
             {
                 eb.HasNoKey();
             });

            builder.Entity<IdentityUserToken<string>>(eb =>
            {
                eb.HasNoKey();
            });


            builder.Entity<Values>().HasData(
                new Values { Id = 1, Name = "Value 101" },
                new Values { Id = 2, Name = "Value 102" },
                new Values { Id = 3, Name = "Value 103" }
            );

            builder.Entity<UserActivity>(x => x.HasKey(ua => new { ua.AppUserId, ua.ActivityId }));

            builder.Entity<UserActivity>()
            .HasOne(u => u.AppUser)
            .WithMany(a => a.UserActivities)
            .HasForeignKey(u => u.AppUserId);

            builder.Entity<UserActivity>()
            .HasOne(a => a.Activity)
            .WithMany(u => u.UserActivities)
            .HasForeignKey(a => a.ActivityId);
        }
    }
}
