using System;

namespace Domain
{
    public class PersonalInfo
    {   
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Upzila { get; set; }
        public string District { get; set; }    
        public string Division { get; set; }
        public string Country { get; set; }
         public string Birthdate { get; set; }
         
    }
}