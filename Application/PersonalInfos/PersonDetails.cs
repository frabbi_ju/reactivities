using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.PersonalInfos
{
    public class PersonDetails
    {
        public class Query : IRequest<PersonalInfo>
        {
            public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Query,PersonalInfo>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<PersonalInfo> Handle(Query request, CancellationToken cancellationToken)
            {
                var person = await _context.PersonalInfos.FindAsync(request.Id);
                return person;
            }
        }
    }
}