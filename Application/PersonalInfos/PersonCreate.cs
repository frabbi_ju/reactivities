using System;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Persistence;

namespace Application.PersonalInfos
{
    public class PersonCreate
    {
        public class Command : IRequest
        {
            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Upzila { get; set; }
            public string District { get; set; }
            public string Division { get; set; }
            public string Country { get; set; }
            public string Birthdate { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                // handler logic  

                var person = new PersonalInfo{
                    Id = request.Id, 
                    FirstName = request.FirstName,
                    LastName = request.LastName, 
                    Upzila = request.Upzila,
                    District = request.District,
                    Division = request.Division, 
                    Country = request.Country,
                    Birthdate = request.Birthdate
                };

                _context.PersonalInfos.Add(person);

                var suc = await _context.SaveChangesAsync() > 0;
                if (suc)
                {
                    return Unit.Value;
                }
                throw new Exception("Problem saving changes");
            }
        }
    }
}