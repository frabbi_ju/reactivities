using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Persistence;

namespace Application.PersonalInfos
{
    public class PersonDelete
    {
        public class Command : IRequest
        {
           public Guid Id { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                // handler logic
                var person = await _context.PersonalInfos.FindAsync(request.Id);

                if (person == null)
                {
                    throw new Exception("No person found");
                }

                _context.Remove(request);

                var suc = await _context.SaveChangesAsync() > 0;
                if (suc)
                {
                    return Unit.Value;
                }
                throw new Exception("Problem saving changes");
            }
        }
    }
}