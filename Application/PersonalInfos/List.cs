using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.PersonalInfos
{
    public class List
    {
        public class Query : IRequest<List<PersonalInfo>> { };

        public class Handler : IRequestHandler<Query, List<PersonalInfo>>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                _context = context;
            }

            public async Task<List<PersonalInfo>> Handle(Query request, CancellationToken cancellationToken)
            {
                var personalInfos = await _context.PersonalInfos.ToListAsync();

                return personalInfos;
            }
        }

    }
}