using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Persistence;

namespace Application.PersonalInfos
{
    public class PersonEdit
    {
        public class Command : IRequest
        {

            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Upzila { get; set; }
            public string District { get; set; }
            public string Division { get; set; }
            public string Country { get; set; }
            public string Birthdate { get; set; }
        }

        public class Handler : IRequestHandler<Command>
        {
            private readonly DataContext _context;
            public Handler(DataContext context)
            {
                this._context = context;
            }

            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                // handler logic   

                var person = await _context.PersonalInfos.FindAsync(request.Id);

                if (person == null)
                {
                    throw new Exception("No person found");
                }

                person.FirstName = request.FirstName ?? person.FirstName;
                person.LastName = request.LastName ?? person.LastName;
                person.Upzila = request.Upzila ?? person.Upzila;
                person.District = request.District ?? person.District;
                person.Division = request.Division ?? person.Division;
                person.Country = request.Country ?? person.Country;
                person.Birthdate = request.Birthdate ?? person.Country;

                var suc = await _context.SaveChangesAsync() > 0;
                if (suc)
                {
                    return Unit.Value;
                }
                throw new Exception("Problem saving changes");
            }
        }
    }
}